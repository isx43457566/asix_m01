Roberto Martínez 
06/04/2020

TASCA 1:

- Processos que un usuari té al sistema i com matar-los:
	ps -U $login
	pkill -U $(grep "^$login:" /etc/passwd | cut -d: -f3)

- Treballs d'impressió que un usuari té al sistema i com matar-los:
	lpq -U $login
	lprm -U $login

- Tasques periòdiques que té un usuari al sistema i com matar-les:
	atq $login
	atrm $num_tasca
	crontab -u $login -l
	crontab -u $login -r

- Fitxers que té un usuari al sistema fora del home i com desar-los
  en un fitxer .tar.gz:
	tar cvzf bk-$login.tgz $(find / -user $login -print 2> /dev/null) 

- Moure tot el home d'un usuari a un fitxer .tar.gz:
	homeDir=$(grep "^$user:" /etc/passwd | cut -d: -f6)
	tar cvzf bk-$login-$homeUser.tgz $homeDir
 
