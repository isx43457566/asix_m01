#! /bin/bash
# Roberto Martínez
# 08/04/2020
# Programa que crea alumnes del curs que rep com a primer argument. Rep
# un segon o més arguments que corresponen als noms del alumnes a donar
# d'alta. A cadascun se li assigna un password random de 8 chars. Aquesta
# info queda enregistrada al fitxer passwd.log amb format login:password.
# Per a generar el password utilitzarem un programa que a l'executar-se
# genera un password random de 8 chars/dígits.
# -----------------------------------------------------------------------
status=0
ERR_NARGS=1
HOME_BASE=/home/inf
SECONDARY_GROUP=users
# 1) validar arguments
if [ $# -lt 2 ]; then
  echo "Error: num args incorrecte"
  echo "Usage: $0 classe alumne[...]"
  exit $ERR_NARGS
fi

# 2) crear grup si cal
group=$1
egrep "^$group:" /etc/group &> /dev/null
if [ $? -ne 0 ]; then
  groupadd $group && echo "Grup $group creat"
else
  echo "Grup $group ja existeix al sistema"
  status=2
fi
egrep "^$SECONDARY_GROUP:" /etc/group &> /dev/null
if [ $? -ne 0 ]; then
  groupadd $SECONDARY_GROUP && echo "Grup $SECONDARY_GROUP creat"
else
  echo "Grup $SECONDARY_GROUP ja existeix al sistema"
  status=3
fi


# 3) crear directori base si cal
mkdir -p $HOME_BASE/$group &> /dev/null && echo "Directori base $HOME_BASE/$group creat"
if [ $? -ne 0 ]; then
  echo "Directori base $HOME_BASE/$group ja existeix al sistema" >> /dev/stderr
  status=4
fi
chgrp $group $HOME_BASE/$group && echo "Directori base $HOME_BASE/$group canviat a grup $group correctament"

# 4) crear usuaris
llistaUsers=$(echo "$*" | cut -d' ' -f2-)
for user in $llistaUsers
do
  password=$(./genera_passwords.py)
  useradd -m -n -g $group -b $HOME_BASE/$group -p $password $user &> /dev/null && echo "Usuari $user creat"
  if [ $? -ne 0 ]; then
    echo "Usuari $user ja existeix al sistema" >> /dev/stderr
    status=5
  else
    echo "$user:$password" >> passwd.log
  fi
done
exit $status

