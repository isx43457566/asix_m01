Roberto Martínez
ASIX1-IM01: ISO
Maig 2020

----------------------------------
PARTICIONS(1): OPERACIONS BÀSIQUES
----------------------------------

1. Particions bàsiques: ntfs i ext4
-----------------------------------
Practicar la creació de particions en format Windows i Linux, muntar i posar dades a les
particions.

Partint de:
● Imatge de VM amb un sistema operatiu F27-Worksatation-mininal instal·lat a sda1 de
4GB.

Implementar:
● Crear una segona partició de 2GB de tipus Windows ntfs, muntar-la i posar-li dades
(almenys 500 MB). Generar un fitxer anomenat access.log dins de la partició i hii
afegiu la data (data i hora) en que l’heu muntat i verificat.
	# fdisk /dev/sda
	-Creem nova particio amb n, escric un 2 quan demana nom partició però per defecte ja sería
	2 també. El primer sector el que ve per defecte i a l'últim posso +2G que és l'espai que volem.
	Comprovo amb p que la taula de partició sigui correcta i amb w ho escric.
	# mkfs.ntfs /dev/sda2 2G
	# mount -t ntfs /dev/sda2 /mnt && date >> access.log
	# dd if=/dev/zero of=dades-windows bs=1k count=50k
	
● Crear una tercera partició de 2GB de tipus ext4, muntar-la i posar-li dades (almenys
500MB). Generar un fitxer anomenat access.log dins de la partició i hi afegiu la data
(data i hora) en que l’heu muntat i verificat.
	# fdisk /dev/sda
	-Faig el mateix procediment que abans però la partició serà la 3.
	# mkfs.ext4 /dev/sda2 2G
        # mount -t ntfs /dev/sda2 /mnt && date >> access.log
        # dd if=/dev/zero of=dades-linux bs=1k count=50k

● Verifiqueu que les dues particions tenen exactament el mateix número de sectors. Si
cal copieu la taula de particions (captura de pantalla o cut&paste).

sda1 (4GB)
F27
------
sda2 (2GB)
Windows ntfs
------
sda3 (2GB)
Linux ext4
------
free space
------
	# fdisk -l


2. Imatge d’una partició / Moure una partició
---------------------------------------------
Practicar la creació d’una imatge amb dd d’una partició i utilitzar també la eina dd per moure
una partició d’un lloc a un altre.

Partint de:
● Imatge de VM amb un sistema operatiu F27-Worksatation-mininal instal·lat. Amb les
tres particions de l’exercici 1.

Implementar:
● Des del sistema actiu F27 generar amb dd un fitxer d’imatge de la partició windows
sda2 (windows.img). Establir el timeout de manera que esperi una selecció i definir
la primera opció de menú com a opció per defecte.
	dd if=/dev/sda2 of=imatge_sda2.img
	# cd /boot/grub2
	# vim grub.cfg
	-editem el fitxer i canvio set timeout=-1 i set default="0"

● Moure amb dd la partició sda3 a sda2.
	dd if=/dev/sda3 of=/dev/sda2

● Muntar la partició ext3 que hi ha ara a sda2, verificar que es pot accedir
apropiadament a les dades i afegir la data (data i hora) al fitxer de registre
access.log. Així quedarà anotat un registre de cada accés a la partició.

sda1 (4GB)
F27
---------
sda2 (2GB)
Linux ext4
----------
sda3 (2GB)
Linux ext4
---------
free space
---------
	# mount /dev/sda2 /mnt && date >> access.log


3. Planxat d’una imatge dd
--------------------------
Practicar a planxar una imatge resident en un fitxer d’imatge tipus .img a una partició física
real.

Partint de:
● Imatge de VM amb un sistema operatiu F27-Worksatation-mininal instal·lat. Amb les
particions de l’exercici 3.

Implementar:
● Utilitzar l’ordre dd per planxar la imatge de windows windows.img a la tercera
partició, sda3.
	# dd if=windows.img of=/dev/sda3

● Muntar la partició i verificar que es pot accedir a les dades. Afegir la data (data i
hora) al fitxer de registre access.log. Així quedarà anotat un registre de cada accés
a la partició.
sda1 (4GB) sda2 (2GB) sda3 (2GB) freee space
F27 Linux ext4 Windows ntfs
	# mount /dev/sda3 /mnt && date >> access.log
	

4. Shrink d’un fs i una partició
--------------------------------
Practicar a engongir una partició. Implica sempre dues passes: a) encongir primer el sistema
de fitxers; b) encongir la partició.
Per encongir el sistema de fitxers s’utilitza resize2fs.
Per encongir una partició simplement s’elimina i es crea de nou. La clau de que funcioni
crrectament és que la nova partició ha de començar exàctament en el mateix sector del disc
que començava abans, sinó les dades es perden!.

Partint de:
● Imatge de VM amb un sistema operatiu F27-Worksatation-mininal instal·lat. Amb les
particions de l’exercici 3.

Implementar:
● Encongir primer el sistema de fitxers Linux ext4 de sda2 a 1GB d’ocupació, usant
resize2fs.
	# resize2fs /dev/sda2 1G

● Encongir la partició a un GB d’ocupació. Cal assegurar-se de que el número de
sectors de la partició és igual o més gran que el número de sectors del sistema de
fitxers. Usar fsdisk.
	# fdisk /dev/sda
	-creem nova partició amb lletra n, comprovo que el primer sector sigui el mateix
	(ho tinc anotat) i al segon sector +1G, comprovo amb p la nova taula de particions i
	amb w guardo els canvis.
	# mkfs.ext4 /dev/sda2 1G

● Muntar la partició i verificar que es pot accedir a les dades. Afegir la data (data i
hora) al fitxer de registre access.log. Així quedarà anotat un registre de cada accés
a la partició.

sda1 (4GB)
F27
----------
sda2 (1GB)
Linux ext4
---------
free sda3 (2GB)
Windows ntfs
-----------
freee space
-----------	
	# mount /dev/sda2 /mnt && date >> access.log


5. Grow d’un fs i una partició
------------------------------
Practicar engrandir un sistema de fitxers i una partició. Primerament cal engrandir la partició,
i a continuació el sistema de fitxers.
Per engrandir una partició cal eliminar-la i crear-la de nou. El secret de l'èxit per no perdre
les dades és que la nova partició ha de començar exactament al mateix punt que
començava anteriorment.
Per engrandir un filesystem s’utilitza resize2fs.

Partint de:
● Imatge de VM amb un sistema operatiu F27-Workstation-mininal instal·lat. Amb les
particions de l’exercici 4.

Implementar:
● Eliminar la partició sda3 de windows.
	# fdisk /dev/sda
	-Premo la d i elimino la particio 3, guardo amb w.

● Eliminar la partició sda2 i crear-la de nou assignant-li 3GB d’espai. Usar fdisk.
	-premo n, serà la partició 2 amb l'ultim sector +3G i tenint en compte que el primer
	sector sigui el mateix, comprovo amb p i guardo amb w.
	# mkfs.ext4 /dev/sda2 3G
	
● Engrandir el sistema de fitxers de sda3 fins a ocupart tot l’espai de la partició, amb
resize2fs.
	# resize2fs /dev/sda3

● Muntar la partició sda2 de Linux ext4 i verificar l’accés a les dades. Afegir la data
(data i hora) al fitxer de registre access.log.
sda1 (4GB)
F27
-----------
sda2 (3GB)
Linux ext4 free space
----------
	# mount /dev/sda2 /mnt && date >> access.log


6. Planxar de nou una imatge
----------------------------
Practicar de nou com planxar una imatge de disc en un fitxer dimatge a una partició física
real.

Partint de:
● Imatge de VM amb un sistema operatiu F27-Worksatation-mininal instal·lat. Amb les
particions de l’exercici 5.

Instal·lar:
● Crear una nova partició sda3 de 2 GB.
	# fdisk /dev/sda
	-el mateix procediment de sempre, partició serà la 3 i l'ultim sector +2G
	# mkfs.ntfs /dev/sda3 2G

● Planxar amb dd la imatge de disc de windows windows.img a la partició sda3.
	# dd if=windows.img of=/dev/sda3	

● Muntar la partició i verificar que es pot accedir a les dades. Afegir la data (data i
hora) al fitxer de registre access.log.

sda1 (4GB)
F27
---------
sda2 (3GB)
Linux ext4
----------
sda3 (2GB)
Windows ntfs
---------
free space
--------
	# mount /dev/sda3 /mnt && date >> access.log


-------------------------------------
PARTICIONS(2): Dos sistemes operatius
-------------------------------------

7. Instal·lar Fedora30-Workstation-Minimal
------------------------------------------
Practicar la instal·lació de un segon sistema operatiu en una nova partició.

Partint de:
● Imatge de VM amb un sistema operatiu F27-Worksatation-mininal instal·lat a sda1 i
amb les particions descrites a l’exercici 6.
Implementar:

● Instal·lar a sda2 un nou sistema operatiu Fedora 30 workstation minimal.
	-Arrenco la màquina virtual amb la imatge del Fedora 30 i segueixo el procés
	d'intal·lació. Creo la partició 2 amb sistema de fitxers ext4 i la monto a / per defecte.
	Segueixo el mateix procediment que a la instal·lació que vaig fer a la practica 5 de grub,
	a la part que instal·lem dos sistemes operatius.

● Reiniciar el sistema i verificar el funcionament del F30.

sda1 (4GB)
F27
----------
sda2 (2GB)
F30
----------
sda3 (2GB)
Windows ntfs
----------
free space
----------
Aquest seria un bon moment per fer un snapshot del disc (de la VM), de manera que
si en les pràctiques posteriors l’espifiem sempre podem tornar a aquesta VM amb els
dos sistemes correctament instal·lats.
	# reboot
	-accedeixo com a root
	- ja fora de la màquina virtual faig l'snapshot
	

8. Grow de F30
--------------
Practicar incrementar la mida d’un sistema operatius.

Partint de:
● Imatge de VM amb un sistema operatiu F27-Worksatation-mininal instal·lat a sda1 i
amb un F30-Workstation-minimal a sda3. amb les particions descrites a l’exercici 7.

Implementar:

● Esborrar sda3.
	# fdisk /dev/sda
	-esborro sda3 amb la opció d i despres ho guardo amb w

● Incrementar la partició de F30 a 3GB (partició i sistema de fitxers).
	# resize2fs /dev/sda2 3G
	# fdisk /dev/sda
	-creo nova partició amb n, será la partició 2 i a l'últim sector +3G,
	comprovo que el primer sector sigui el mateix de abans i guardo amb w.
	# mkfs.ext4 /dev/sda2 3G

● Reiniciar el sistema i verificar el funcionament del F30.

sda1 (4GB)
F27
----------
sda2 (3GB)
F30
----------
freee space
----------
	# reboot
	-accedeixo com a root


9. Shrink de F30
----------------
Practicar a reduir un sistema operatiu (filesystem i partition).

Partint de:
● Imatge de VM amb un sistema operatiu F27-Worksatation-mininal i un
F30-workstation-minimal. instal·lat. Amb les particions de l’exercici 8.

Implementar:
● Reiniciar el host amb F27.
	# reboot
	-accedeixo a f27

● Empetitir F30 a 2GB (fs i partició)
	# resize2fs /dev/sda2 2G
	# fdisk /dev/sda
	-segueixo el mateix procediment que en exercicis anterior, en aquest cas partició 2, tenint
	molt en compte sempre que el primer sector sigui el mateix.
	# mkfs.ext4 /dev/sda2 2G

● Reboot i inicialitzar F30, verificar que arrenca i funciona.
	# reboot
	-accedeixo com a root
	
sda1 (4GB)
F27
----------
sda2 (2GB)
F30
----------
freee space
----------


10. Moure un sistema operatiu.
------------------------------
Practicar moure el sistema operatiu F30 que està a sda2 a sda3.

Partint de:
● Imatge de VM amb un sistema operatiu F27-Worksatation-mininal i un
F30-workstation-minimal. instal·lat. Amb les particions de l’exercici 9.

Implementar:
● Crear sda3 de 3GB. Assegurar-se de que conté tants sectors com sda2.
	# fdisk /dv/sda3
	-mateix procediment amb n, vigilo primer sector i segon +3G, comprovo p
	i guardo w.
	# mkfs.ext4 /dev/sda3 3G

● Amb l’ordre dd moure el sistema operatiu F30 de sda2 a sda3.
	# dd if=/dev/sda2 of=/dev/sda3

● Planxar amb zeros tota la partició sda2 per destruir totalment el seu contingut, usant
l’ordre dd.
sda1 (4GB)
F27
----------
sda2 (3GB)
zeros
---------
sda3 (3GB)
F30
----------
free space
	# dd if=/dev/zero of=/dev/sda2 bs=1k count=3M 

	
11. Engegar F30
---------------
Practicar com engegar un sistema operatu quan el grub ha quedat destruït i no pot arrencar.

Partint de:
● Imatge de VM amb un sistema operatiu F27-Worksatation-mininal i un
F30-workstation-minimal instal·lat. Amb les particions de l’exercici 10.

Implementar:
● Com que el grub del MBR apunta on estava abans el F30, ara el sistema no arrenca!
● Aconseguir arrencar manualment el sistema operatiu, tant pot ser F27 com F30.
	al mode comanda de grub faig les següents instruccions:
	set root=(hd0,msdos3)
	set prefix=(hd0,msdos3)/boot/grub2
	insmod normal
	normal
	boot

● Establir que el sistema operatiu amb el Grub que mana és el F30.
	# grub2-install /dev/sda (estic al f30)

● Reboot i verificar que el menú del grub funciona i que es pot arrencar el F30.
	# reboot
	-accedeixo al f30 com a root


12. Crear una partició de swap
------------------------------
Practicar la creació d’una partició de swap.

Partint de:
● Imatge de VM amb un sistema operatiu F27-Worksatation-mininal i un
F30-workstation-minimal. instal·lat. Amb les particions de l’exercici 11.

Implementar:
● Esborrar sda2 i crear-la de nou de 1GB.
	# fdisk /dev/sda
	-esborro amb d la partició 2 i creo amb n la nova amb el segon sector +1G i tenint 
	en compte el primer, com sempre. Comprovo amb p i guardo amb w.	
	
● Assignar-li format de swap.
	# mkswap /dev/sda2

● Configurar F30 per usar la swap en iniciar-se.
	# vim /etc/fstab
	-afegim la swap al fitxer fstab així es montarà sempre al iniciar el sistema
	/dev/sda2   swap    swap    defaults    0  0  (la línia ha de quedar així)

● Reboot i verificar que la swap està activa.
sda1 (4GB)
F27
----------
sda2 (1GB)
swap
---------
free sda3 (3GB)
F30
---------
freee space
---------
	# reboot
        -accedeixo com a root
        # swapon -s


13. Moure una partició a l’esquerra
-----------------------------------
Practicar com moure una partició modificant el seu punt de començament. Fins ara hem vist
que podem fer grow i shrink de particions sempre i quant es respecti el punt inicial. També
podem fer dd i copiar-la on sigui si hi ha espai suficient. Ara falta veure com moure la
partició a l’esquerra quan no hi cap tota.

Partint de:
● Imatge de VM amb un sistema operatiu F27-Worksatation-mininal i un
F30-workstation-minimal. instal·lat. Amb les particions de l’exercici 12.

Implementar:
● Entre sda2 (swap) i sda3 (F30) hi ha espai lliure, 1GB aproximadament. No podem
moure amb dd la partició perquè es trepitja a ella mateixa.
● Volem posar el sistema operatiu F30 i la partició sda3 que sigui contigua a sda2.
● Per fer-ho cal usar una eina gràfica com per exemple gparted.
● Descarregar la imatge live de gparted.
● Arrencar de nou la màquina virtual afegint el CD/DVD live de gparted.
● Visualment usant gparted realitzar els canvis que es demanen
sda1 (4GB)
F27
----------
sda2 (3GB)
F30
----------
free space
----------
	-accedeixo a la màquina virtual carregant el cdrom de la imatge del gparted i amb
        aquesta eina faig aquesta operació fixant-me sobretot en que les mides de les particions
        siguin les corresponents.


14. Et voilà
------------
Finalment restaurar l’arrancada de F27 amb un menú del grub que permeti engegar els dos
sistemes operatius.

Partint de:
● Imatge de VM amb un sistema operatiu F27-Worksatation-mininal i un
F30-workstation-minimal. instal·lat. Amb les particions de l’exercici 13.

Implementar:
● Generar de nou l’arrencada establint que la partició que mana del grub és la del F27.
	-arrenco el f27
	# grub2-install /dev/sda

● El menú d’arrencada ha de permetre engegar els dos sistemes operatius.
	# grub2-mkconfig -o grub.cfg
