Roberto Martínez Conejero
ASIX 1 - M01 ISO

UNDER CONSTRUCTION
------------------

Pràctiques proposades:
----------------------
1. Snapshot: generar un snapshot amb tres serveis activats i observar que estan actius
o no segons si s’està al target del snapshot o no.
2. Service WantedBy: lligar un servei a més d’un target.
3. Target propi: Generar un target propi basat en multi-user.target que carregi un parell
de serveis propis del target.
4. Serveis propis: Crear serveis propis que facin accions trivils per practicar systemd.


Snapshot
--------
● Al target multi-user.target assegurar-se de tenir disabled i stoped els serveis
gpm.service, xinetd.service i vsftpd.service.
	# systemctl is-enabled xinetd.service gdm.service vsftpd.service
	# systemctl is-active xinetd.service gdm.service vsftpd.service

● Activar aquests tres serveis amb start (sense fer el enable).
	# systemctl start xinetd.service gdm.service vsftpd.service

● Definir un snapshot de nom test-serveis.snapshot.
	# vim /usr/lib/systemd/system/test-serveis.snapshot

● Com a mesura extra activar el target rescue.target amb isolate. Observar que els tres
serveis indicats estan apagats i disabled.
	# systemctl isolate rescue.target
	# systemctl is-active xinetd.service gdm.service vsftpd.service 
	# systemctl is-enabled xinetd.service gdm.service vsftpd.service

● Activar el target del snapshot amb isolate. Observar que ara que els tres serveis
estan actius amb l’ordre show. Observar que el target test-serveis.snapshot també
està actiu.
	# systemctl isolate test-serveis.snapshot
	# systemctl show xinetd.service
	# systemctl show gdm.service
	# systemctl show vsftpd.service
	# systemctl is-active test-serveis.snapshot

● Activar el target multi-user.target amb isolate i observar que els tres serveis tornen a
star stop. Verificar la configuració amb ordres is-active, is-enable, status i show.
	# systemctl isolate test-serveis.snapshot
	# systemctl is-active xinetd.service gdm.service vsftpd.service
	# systemctl is-enabled xinetd.service gdm.service vsftpd.service
	# systemctl status xinetd.service gdm.service vsftpd.service
        # systemctl show xinetd.service
        # systemctl show gdm.service
        # systemctl show vsftpd.service

● Llistar les característiques del snapshot amb l’opció show.
	# systemctl show test-serveis.snapshot


Service WantedBy
----------------
● Activar el servei multi-user.target amb isolate.
	# systemctl isolate multi-user.target

● Editar el servei gpm.service i xinetd.service i afegir a la secció installation:
WantedBy=rescue.target. Ara els serveis son requerits tant per el target
multi-user.target com per rescue.target.
	# vim /usr/lib/systemd/system/gdm.service
	# vim /usr/lib/systemd/system/xinetd.service
	als dos fitxer de serveis afeigeixo rescue.target al WantedBy
	WantedBy=multi-user.target rescue.target

● Si cal recordar de fer el systemctl daemon-reload.
	# systemctl daemon-reload

● Fer disable / enable dels serveis gpm.service i xinetd.service. Observar que es creen
els symlinks i que s’esborren. Dos en cada cas.
	# systemctl disable gdm.service xinetd.service
	# systemctl enable gdm.service xinetd.service

● Deixar els dos serveis enabled i observar el llistat de
/etc/systemd/system/rescue.target.wants on ara apareixen els dos serveis enabled.
	ls -l /etc/systemd/system/rescue.target.wants

● Activar el target rescue.target amb isolate. Observar amb status que els dos serveis
son actius. Verificar també amb is-active i is-enabled.
	# systemctl isolate rescue.target
	# systemctl status gdm.service xinetd.service
	# systemctl is-active gdm.service xinetd.service
	# systemctl is-enabled gdm.service xinetd.service

● Activar el servei multi-user.target i repetir les comprovacions fetes en el cas de
rescue.target.
	# systemctl isolate multi-user.target
        # systemctl status gdm.service xinetd.service
        # systemctl is-active gdm.service xinetd.service
        # systemctl is-enabled gdm.service xinetd.service

● Editar els dos fitxers de servei i restablir la configuració inicial. Si cal recordar de fer
el systemctl daemon-reload.
	# vim /usr/lib/systemd/system/gdm.service
        # vim /usr/lib/systemd/system/xinetd.service
        als dos fitxer de serveis esborro rescue.target al WantedBy i deixo multi-user.target
        WantedBy=multi-user.target


Crear target propi
------------------
Crear un target propi anomenat test-target.target que es basa amb multi-user.target
mes els serveis test-gpm.service i test-xinetd.service

● Copiar graphical.target de /usr/lib/system/system al mateix directori amb el nom
test-target.target. Eliminar el wants.
	cp /usr/lib/systemd/system/graphical.target usr/lib/systemd/test-graphical.target
	rm /usr/lib/systemd/system/graphical.target.wants	

● Copiar els serveis gpm.service i xinetd.service (atmbé a /usr/lib/systemd/system) i
anomenar-los test-gpm.service i test-xinetd.service. Editar-los per indicar que són
únicament WantedBy=test-target.target.3
	# cp /usr/lib/systemd/system/gdm.service usr/lib/systemd/test-gdm.service
	# cp /usr/lib/systemd/system/xinetd usr/lib/systemd/test-xinetd.service
	# vim usr/lib/systemd/test-gdm.service
	# vim usr/lib/systemd/test-xinetd.service
	edito última línia com possa a l'enunciat

● Fer enable dels nous serveis test-gpm.target i test-xinetd.target. Observar el llistat de
/etc/systemd/system/test-target.wants on ara apareixen els dos serveis que requereix
el nou target. Els serveis no estan encara start, però si enabled, verificar-ho amb
is-active i is-enabled.
	# systemctl enable test-gpm.service test-xinetd.service
	# ls -l /etc/systemd/system/test-target.wants
	# systemd is-active test-gdm.service test-xinetd.service

● Activar el target test-target.target amb isolate i observar amb is-active i
list-dependencies que s’està a aquest target i que els serveis son active i el llistat de
dependències.
	# systemctl isolate test-target.target
	# systemctl is-active test-target.target
	# systemctl list-dependencies test-target.target

● Activar el mode multi-user.target i observar que els serveis test-gpm.service i
test-xinetd.service no són actius. Verificar-ho amb is-active.
	# systemctl isolate multi-user.target
        # systemctl is-active test-gdm.service test-xinetd.service


